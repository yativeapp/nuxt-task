import Router from 'vue-router'

export function createRouter(ssrContext, createDefaultRouter) {
  const defaultRouter = createDefaultRouter(ssrContext)
  return new Router({
    ...defaultRouter.options,
    routes: fixRoutes(defaultRouter.options.routes)
  })
}

function fixRoutes(defaultRoutes) {

  let login = defaultRoutes.find(route => route.name == 'login');

  defaultRoutes.push({
    path: "/تسجيل-دخول",
    component: login.component,
    name: "login-ar"
  });
  return defaultRoutes;
}
